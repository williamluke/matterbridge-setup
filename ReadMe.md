## Matter-bridge

Edit `./matterbridge/matterbridge.toml` to configure Matterbridge.
Edit `docker-compose.yml` 
`docker-compose up -d`

## Mattermost
### Helm
```
helm install my-release mattermost/mattermost-team-edition -f custom-values.yaml

# You can easily connect to the remote instance from your browser. Forward the webserver port to localhost:8065
kubectl port-forward --namespace default $(kubectl get pods --namespace default -l "app.kubernetes.io/name=mattermost-team-edition,app.kubernetes.io/instance=my-release" -o jsonpath='{ .items[0].metadata.name }') 8080:8065

# To expose Mattermost via an Ingress you need to set host and enable ingress.
helm install --set host=mattermost.yourdomain.com --set ingress.enabled=true stable/mattermost-team-edition
```

### kubectl
```
minikube start 
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.46.0/deploy/static/provider/cloud/deploy.yaml
kubectl create ns mattermost-operator
kubectl apply -n mattermost-operator -f https://raw.githubusercontent.com/mattermost/mattermost-operator/master/docs/mattermost-operator/mattermost-operator.yaml
kubectl create ns mysql-operator
kubectl apply -n mysql-operator -f https://raw.githubusercontent.com/mattermost/mattermost-operator/master/docs/mysql-operator/mysql-operator.yaml
kubectl create ns minio-operator
kubectl apply -n minio-operator -f https://raw.githubusercontent.com/mattermost/mattermost-operator/master/docs/minio-operator/minio-operator.yaml
kubectl create ns mattermost
# kubectl apply -n mattermost -f ./mattermost-license-secret.yml
kubectl apply -n mattermost -f ./mattermost-installation.yml

kubectl -n mattermost get mm -w

kubectl -n mattermost get ingress


```

Restoring an Existing Mattermost MySQL Database
[Docs](https://docs.mattermost.com/install/install-kubernetes-mattermost.html#restoring-an-existing-mattermost-mysql-database)

```
kubectl apply -n mattermost -f ./restore.yaml
```


docker run -ti --mount "type=bind,source=$(pwd)/matterbridge,target=/etc/matterbridge" 42wim/matterbridge
